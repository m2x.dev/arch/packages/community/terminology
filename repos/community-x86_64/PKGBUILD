# Maintainer: Ronald van Haren <ronald@archlinux.org>
# Maintainer: Carsten Haitzler <raster@archlinux.org>
# Contributor: Doug Newgard <scimmia22 at outlook dot com>
# Contributor: Paul Ezvan <paul@ezvan.fr>

pkgname=terminology
pkgver=1.11.0
pkgrel=1
pkgdesc="EFL based terminal emulator"
arch=('x86_64')
url="https://www.enlightenment.org/about-terminology"
license=('BSD')
depends=('efl')
makedepends=('meson')
source=("https://download.enlightenment.org/rel/apps/${pkgname}/${pkgname}-${pkgver}.tar.xz")
sha256sums=('4fd884bd2ffbbc86d87163063074fbd969be04b17bb8d7e23cd1f6708fd86a2d')

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  export CFLAGS="${CFLAGS} -fvisibility=hidden"
  meson builddir --prefix=/usr
  ninja -C builddir
}

package(){
  cd "${srcdir}/${pkgname}-${pkgver}"

  DESTDIR="${pkgdir}" ninja -C builddir install

  # install license files
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/COPYING" \
        "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}
